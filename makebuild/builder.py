# builder.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import, print_function, with_statement
from .constants import *
from .cmdline import CommandLineBuilder
from .header import HeaderBuilder
from .utils import *
import os
import re

__all__ = ["MakefileBuilder"]

INCLUDE_REGEX = re.compile(r'^#\s*include\s*"([^\n]+)"')

OBJECT_RULE = """\
FLAGS = $(STD_FLAGS) %s $(INCLUDES) %s
%s: %s $(BUILD_DIR) %s
\t%s
"""

TARGET_RULE = """\
FLAGS =
%s: %s
\t%s
"""


class MakefileBuilder(object):
    def __init__(self, config, filename):
        self.config = config
        self.filename = filename

    def make(self):
        status("generating makefile '%s'." % self.filename)
        lines = []

        header_builder = HeaderBuilder(self.config, lines)
        header_builder.build()

        objects = set()
        for name, target in self.config.targets.items():
            cmdline_builder = CommandLineBuilder(self.config, lines, target['type'] == 'shared-lib')

            status("getting sources for target '%s'." % name)
            c_sources, cxx_sources = self.get_sources(name)
            c_cmdline = cmdline_builder.build_c()

            status("writing c source rules.", 2)
            for source in c_sources:
                status("writing rule for c source '%s'." % source, 2)
                headers = self.get_headers(source)
                output = "%s/%s.o" % (self.config.output_dir, source.replace(os.path.sep, '_'))
                objects.add(output)
                lines.append(OBJECT_RULE %
                        (' '.join(target['include']), ' '.join(target['compile-flags']),
                            output, source, ' '.join(headers), c_cmdline))

            cxx_cmdline = cmdline_builder.build_cxx()

            status("writing cxx source rules.", 2)
            for source in cxx_sources:
                pass

            print(c_sources)
            print(cxx_sources)
            print(objects)
            print(target)

            status("writing rule for linked object '%s'." % name)
            if target['type'] in ('executable', 'shared-lib'):
                if cxx_sources:
                    cmdline = cmdline_builder.build_ldxx()
                else:
                    cmdline = cmdline_builder.build_ld()

                lines.append(TARGET_RULE  % (name, ' '.join(objects), cmdline))
            elif target['type'] == 'static-lib':
                cmdline = cmdline_builder.build_ar()
                lines.append(TARGET_RULE % (name, ' '.join(objects), cmdline))
            else:
                error("invalid target type: '%s'." % target['type'])

        try:
            with open(self.filename, 'w') as fh:
                fh.write('\n'.join(lines))
                fh.write('\n')
        except (OSError, IOError) as err:
            error("unable to write to '%s': %s." % (self.filename, err))

    def get_headers(self, source):
        status("reading headers for '%s'." % source, 2)
        headers = []

        try:
            with open(source, 'r') as fh:
                for line in fh.readlines():
                    match = INCLUDE_REGEX.match(line)
                    if match:
                        header = match.group(1)
                        headers.append(header)
                        if not self.is_c_header(header) and not self.is_cxx_header(header):
                            warning("included file is not recognized as a header: '%s'." % header)
        except (OSError, IOError) as err:
            error("unable to read source file '%s'." % source)

        return set(headers)

    def get_sources(self, target_name):
        c = set()
        cxx = set()

        for source in self.config.sources:
            if not os.path.exists(source):
                warning("source path '%s' does not exist." % source)
            elif os.path.isdir(source):
                for dirpath, dirnames, filenames in os.walk(source):
                    if os.path.isabs(dirpath):
                        warning("source directory is an absolute path: '%s'." % dirpath)
                    for filename in filenames:
                        full_filename = os.path.join(dirpath, filename)
                        self.add_to_sources(full_filename, c, cxx)
            else:
                self.add_to_sources(source, c, cxx)

        for source in self.config.targets[target_name]['sources-include']:
            if not os.path.exists(source):
                warning("source path '%s' does not exist." % source)
            elif os.path.isdir(source):
                for dirpath, dirnames, filenames in os.walk(source):
                    if os.path.isabs(dirpath):
                        warning("source directory is an absolute path: '%s'." % dirpath)
                    for filename in filenames:
                        full_filename = os.path.join(dirpath, filename)
                        self.add_to_sources(full_filename, c, cxx)
            else:
                self.add_to_sources(source, c, cxx)

        for source in self.config.targets[target_name]['sources-exclude']:
            if source in c:
                c.remove(source)
            if source in cxx:
                cxx.remove(source)

        if not c and not cxx:
            error("no source files found.")

        return c, cxx

    def add_to_sources(self, filename, c, cxx):
        if self.is_c_source(filename):
            c.add(filename)
        elif self.is_cxx_source(filename):
            cxx.add(filename)

    def is_c_source(self, filename):
        for ext in self.config.c_ext:
            if filename.endswith(ext):
                return True
        return False

    def is_cxx_source(self, filename):
        for ext in self.config.cxx_ext:
            if filename.endswith(ext):
                return True
        return False

    def is_c_header(self, filename):
        for ext in self.config.c_hdr:
            if filename.endswith(ext):
                return True
        return False

    def is_cxx_header(self, filename):
        for ext in self.config.cxx_hdr:
            if filename.endswith(ext):
                return True
        return False

