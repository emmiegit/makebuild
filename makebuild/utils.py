# utils.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import, print_function
from .constants import *
import traceback

__all__ = ["error", "warning", "status", "set_verbosity"]

class StatusLogger(object):
    def set_verbosity(self, verbosity):
        self.verbosity = verbosity

    def error(self, message, tb=False):
        if tb:
            print(traceback.format_exc())

        print("%sError:%s %s" % (COLOR_RED, COLOR_RESET, message))
        exit(1)

    def warning(self, message, tb=False):
        if tb:
            print(traceback.format_exc())

        print("%sWarning:%s %s" % (COLOR_YELLOW, COLOR_RESET, message))

    def status(self, message, level=1):
        if self.verbosity >= level:
            print(message)

logger = StatusLogger()
error = logger.error
warning = logger.warning
status = logger.status
set_verbosity = logger.set_verbosity

