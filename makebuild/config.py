# config.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import, print_function, with_statement
from .constants import *
from .utils import *
import json
import pprint

__all__ = ["Configuration"]

CONFIG_SECTIONS = ('project', 'build', 'misc')
TARGET_TYPES = ('executable', 'static-lib', 'shared-lib')
DEFAULT_SOURCE_TYPE = 'c'


class Configuration(object):
    def __init__(self, filename):
        status("reading configuration file '%s'." % filename)

        try:
            with open(filename, "r") as fh:
                self.raw = json.load(fh)
        except (IOError, OSError) as err:
            error("unable to open '%s': %s." % filename, err, tb=True)

        status("raw data from json:\n%s" % pprint.pformat(self.raw), 3)

        for section in CONFIG_SECTIONS:
            status("reading section '%s'." % section)
            try:
                getattr(self, "check_%s" % section)(self.raw.get(section, {}))
            except KeyError as key:
                self.missing_element(key, section)

        del self.raw

    def check_project(self, config):
        self.name = config['name']
        self.version = config['version']
        self.header = config.get('header', None)

        self.correct_type('project', 'name', str)
        self.correct_type('project', 'version', str)
        self.correct_type('project', 'header', str, True)

    def check_build(self, config):
        self.cc = config.get('c-compiler', None)
        self.cxx = config.get('cxx-compiler', None)
        self.ar = config.get('archiver', None)
        self.make = config.get('make', None)
        self.shell = config.get('shell', None)
        self.output_dir = config.get('output_dir', 'build')

        self.correct_type('build', 'cc', str, True)
        self.correct_type('build', 'cxx', str, True)
        self.correct_type('build', 'ar', str, True)
        self.correct_type('build', 'make', str, True)
        self.correct_type('build', 'shell', str, True)
        self.correct_type('build', 'output_dir', str)

        self.includes = config.get('include-dirs', [])
        self.flags = config.get('flags', [])
        self.compile_flags = config.get('compile-flags', [])
        self.link_flags = config.get('link-flags', [])
        self.debug_flags = config.get('debug-flags', ['-g'])
        self.sources = config.get('sources', ['src'])

        self.correct_type('build', 'includes', list)
        self.correct_type('build', 'flags', list)
        self.correct_type('build', 'compile_flags', list)
        self.correct_type('build', 'link_flags', list)
        self.correct_type('build', 'debug_flags', list)
        self.correct_type('build', 'sources', list)

        dictionary = config.get('c', {})
        self.c_include = dictionary.get('include-dirs', [])
        self.c_flags = dictionary.get('flags', [])
        self.c_compile_flags = dictionary.get('compile-flags', [])
        self.c_link_flags = dictionary.get('link-flags', [])

        self.correct_type('build', 'c_include', list)
        self.correct_type('build', 'c_flags', list)
        self.correct_type('build', 'c_compile_flags', list)
        self.correct_type('build', 'c_link_flags', list)

        dictionary = config.get('cxx', {})
        self.cxx_include = dictionary.get('include-dirs', [])
        self.cxx_flags = dictionary.get('flags', [])
        self.cxx_compile_flags = dictionary.get('compile-flags', [])
        self.cxx_link_flags = dictionary.get('link-flags', [])

        self.correct_type('build', 'cxx_include', list)
        self.correct_type('build', 'cxx_flags', list)
        self.correct_type('build', 'cxx_compile_flags', list)
        self.correct_type('build', 'cxx_link_flags', list)

        self.check_targets(config.get('targets', {}))

    def check_targets(self, targets):
        self.targets = {}

        try:
            for name, config in targets.items():
                status("reading build target '%s'." % name)
                section = 'target.%s' % name
                target = {}

                target['sources-include'] = config.get('sources-include', [])
                target['sources-exclude'] = config.get('sources-exclude', [])
                target['output'] = config.get('output', name)
                target['type'] = config['type']

                self.correct_type_dict(section, 'sources-include', target, list)
                self.correct_type_dict(section, 'sources-exclude', target, list)
                self.correct_type_dict(section, 'output', target, str)
                self.correct_type_dict(section, 'type', target, str)

                if target['type'] not in TARGET_TYPES:
                    error("invalid target type for '%s': %s." % (name, target['type']))

                target['include'] = config.get('include-dirs', [])
                target['flags'] = config.get('flags', [])
                target['compile-flags'] = config.get('compile-flags', [])
                target['link-flags'] = config.get('link-flags', [])

                self.correct_type_dict(section, 'include', target, list)
                self.correct_type_dict(section, 'flags', target, list)
                self.correct_type_dict(section, 'compile-flags', target, list)
                self.correct_type_dict(section, 'link-flags', target, list)

                dictionary = config.get('c', {})
                target['c-include'] = dictionary.get('include-dirs', [])
                target['c-flags'] = dictionary.get('flags', [])
                target['c-compile-flags'] = dictionary.get('compile-flags', [])
                target['c-link-flags'] = dictionary.get('link-flags', [])

                self.correct_type_dict(section, 'c-include', target, list)
                self.correct_type_dict(section, 'c-flags', target, list)
                self.correct_type_dict(section, 'c-compile-flags', target, list)
                self.correct_type_dict(section, 'c-link-flags', target, list)

                dictionary = config.get('cxx', {})
                target['cxx-include'] = dictionary.get('include-dirs', [])
                target['cxx-flags'] = dictionary.get('flags', [])
                target['cxx-compile-flags'] = dictionary.get('compile-flags', [])
                target['cxx-link-flags'] = dictionary.get('link-flags', [])

                self.correct_type_dict(section, 'cxx-include', target, list)
                self.correct_type_dict(section, 'cxx-flags', target, list)
                self.correct_type_dict(section, 'cxx-compile-flags', target, list)
                self.correct_type_dict(section, 'cxx-link-flags', target, list)

                self.targets[name] = target
        except KeyError as key:
            self.missing_element('targets.%s' % name, key)

        if len(self.targets) == 0:
            warning('no targets specified.')

    def check_misc(self, config):
        self.default_src_type = config.get('default', DEFAULT_SOURCE_TYPE).lower()
        self.c_ext = config.get('c-sources', C_EXTENSIONS)
        self.c_hdr = config.get('c-headers', C_HEADERS)
        self.cxx_ext = config.get('cxx-sources', CXX_EXTENSIONS)
        self.cxx_hdr = config.get('cxx-headers', CXX_HEADERS)

        self.correct_type('misc', 'default_src_type', str)
        self.correct_type('misc', 'c_ext', list)
        self.correct_type('misc', 'c_hdr', list)
        self.correct_type('misc', 'cxx_ext', list)
        self.correct_type('misc', 'cxx_hdr', list)

        if self.default_src_type not in ('c', 'cxx'):
            error("default source type is not 'c' or 'cxx', is '%s'." % self.default_src_type)

        if len(self.c_ext) == 0:
            warning("list of c source extensions is empty.")

        if len(self.c_hdr) == 0:
            warninig("list of c header extensions is empty.")

        if len(self.cxx_ext) == 0:
            warning("list of c++ source extensions is empty.")

        if len(self.cxx_hdr) == 0:
            warninig("list of c++ header extensions is empty.")

    def correct_type(self, section, name, expected_type, none_ok=False):
        self.correct_type_obj(section, name, getattr(self, name), expected_type, none_ok)

    @staticmethod
    def correct_type_dict(section, name, dictionary, expected_type, none_ok=False):
        Configuration.correct_type_obj(section, name, dictionary[name], expected_type, none_ok)

    @staticmethod
    def correct_type_obj(section, name, obj, expected_type, none_ok=False):
        if type(obj) != expected_type and not (obj is None and none_ok):
            error("expected element '%s.%s' to be %s%s, not %s." %
                    (section, name, PYTHON_TYPE_NAMES[expected_type],
                     " or null" if none_ok else "", PYTHON_TYPE_NAMES[type(obj)]))

    @staticmethod
    def missing_element(name, section):
        error("required element '%s.%s' missing." % (section, name))

