# cmdline.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import
from .utils import *

__all__ = ["CommandLineBuilder"]


class CommandLineBuilder(object):
    def __init__(self, config, lines, shared):
        self.config = config
        self.lines = lines
        self.shared = shared

    def build_c(self):
        if self.shared:
            extra = ' -fpic'
        else:
            extra = ''

        return "$(ECHO_CC) $(FLAGS) $(CFLAGS) $(EXTRA_FLAGS)%s -c -o $@ $<" % extra

    def build_cxx(self):
        if self.shared:
            extra = ' -fpic'
        else:
            extra = ''

        return "$(ECHO_CXX) $(FLAGS) $(CXXFLAGS) $(EXTRA_FLAGS)%s -c -o $@ $<" % extra

    def build_ld(self):
        if self.shared:
            extra = ' -fpic'
        else:
            extra = ''

        return "$(ECHO_LD) $(FLAGS) $(LDFLAGS) $(EXTRA_FLAGS)%s -c -o $@ $<" % extra

    def build_ldxx(self):
        if self.shared:
            extra = ' -fpic'
        else:
            extra = ''

        return "$(ECHO_LDXX) $(FLAGS) $(LDFLAGS) $(LDXXFLAGS) $(EXTRA_FLAGS)%s -c -o $@ $<" % extra

    def build_ar(self):
        return "$(ECHO_AR) $(FLAGS) $(ARFLAGS) $@ $^"

