# constants.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

import sys

__all__ = [
    "PYTHON_TYPE_NAMES",
    "COLOR",
    "COLOR_RED",
    "COLOR_YELLOW",
    "COLOR_RESET",
    "C_EXTENSIONS",
    "C_HEADERS",
    "CXX_EXTENSIONS",
    "CXX_HEADERS",
]

PYTHON_TYPE_NAMES = {
    str: 'a string',
    list: 'a list',
    dict: 'an object',
    type(None): 'null',
}

COLOR = sys.stdin.isatty()
COLOR_RED = "\x1b[31m" if COLOR else ""
COLOR_YELLOW = "\x1b[33m" if COLOR else ""
COLOR_RESET = "\x1b[0m" if COLOR else ""

C_EXTENSIONS = ['c']
C_HEADERS = ['h']
CXX_EXTENSIONS = ['cc', 'cxx', 'cpp', 'c++', 'C']
CXX_HEADERS = ['hh', 'hxx', 'hpp', 'h++', 'H', 'ii', 'ixx', 'ipp', 'inl', 'txx', 'tpp', 'tpl']

