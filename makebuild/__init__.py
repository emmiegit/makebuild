#!/usr/bin/env python
#
# __init__.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import
from .builder import MakefileBuilder
from .config import Configuration
from .utils import *
import argparse
import pprint

__all__ = ["main"]
__license__ = "GPL2+"


PROGRAM_NAME = "makebuild"
PROGRAM_VERSION = "0.0.1"

PROGRAM_DESCRIPTION = """\
This program generates portable Makefiles for C/C++ projects based on a JSON template in a way that avoids having to manually write out boilerplate. It seeks to provide many of the nice features that build tools provide, such as recompilation on header updates.
"""

HELP_VERBOSE = "Run verbosely, printing out lots of information when generating the Makefile. Specify more vs for higher verbosity."

HELP_OUTPUT_FILE = "The file to generate. The default is \"Makefile\"."

HELP_CONFIG_FILE = "The configuration file to use. This JSON file defines various properties about the resultant Makefile."


def main(argv=[__file__]):
    argparser = argparse.ArgumentParser(description=PROGRAM_DESCRIPTION)
    argparser.add_argument('-v', '--verbose', action='count', default=0, help=HELP_VERBOSE)
    argparser.add_argument('-o', '--output', action='store', default='Makefile', help=HELP_OUTPUT_FILE)
    argparser.add_argument('config', help=HELP_CONFIG_FILE)
    args = argparser.parse_args()

    set_verbosity(args.verbose)
    config = Configuration(args.config)
    status("created configuration:\n%s" % pprint.pformat(config.__dict__), 3)
    builder = MakefileBuilder(config, args.output)
    builder.make()

