# header.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import absolute_import
from .utils import *

__all__ = ["HeaderBuilder"]

HEADER_0 = """\
#
# Makefile
#
# DO NOT EDIT - Automatically generated
#
"""

HEADER_1 = """\
.POSIX:
.PHONY: all debug clean

V = 0

ECHO_CC_0     = @echo 'CC    $@'; $(CC)
ECHO_CC_1     = $(CC)
ECHO_CC       = $(ECHO_CC_$(V))

ECHO_CXX_0    = @echo 'CXX   $@'; $(CXX)
ECHO_CXX_1    = $(CXX)
ECHO_CXX      = $(ECHO_CXX_$(V))

ECHO_LD_0     = @echo 'LD    $@'; $(CC)
ECHO_LD_1     = $(CC)
ECHO_LD       = $(ECHO_LD_$(V))

ECHO_LDXX_0   = @echo 'LDXX  $@'; $(CXX)
ECHO_LDXX_1   = $(CXX)
ECHO_LDXX     = $(ECHO_LDXX_$(V))

ECHO_AR_0     = @echo 'AR    $@'; $(AR)
ECHO_AR_1     = $(AR)
ECHO_AR       = $(ECHO_AR_$(V))

ECHO_SHELL_0  = @echo 'SHELL $@'; $(SHELL)
ECHO_SHELL_1  = $(SHELL)
ECHO_SHELL    = $(ECHO_SHELL_$(V))

ECHO_MAKE_0   = @echo 'MAKE  $@'; $(MAKE)
ECHO_MAKE_1   = $(MAKE)
ECHO_MAKE     = $(ECHO_MAKE_$(V))
"""

HEADER_2 = """\

all: %s

debug: EXTRA_FLAGS += %s
debug: %s

clean:
\t@echo 'CLEAN'
\t@rm -f $(BUILD_DIR)/*

$(BUILD_DIR):
\t@echo 'MKDIR $@'
\t@mkdir -p $@
"""


class HeaderBuilder(object):
    def __init__(self, config, lines):
        self.config = config
        self.lines = lines

    def build(self):
        self.lines.append(HEADER_0)

        if self.config.header:
            status("writing makefile header from '%s'." % self.config.header)
            try:
                with open(self.config.header, 'r') as fh:
                    for line in fh.readlines():
                        lines.append("# %s" % line.rstrip())
            except (OSError, IOError) as err:
                error("unable to read from text header '%s': %s." % (self.config.header, err))

        self.lines.append(HEADER_1)

        if self.config.cc:
            status("setting c compiler to '%s'." % self.config.cc, 2)
            self.lines.append("CC = %s" % self.config.cc)

        if self.config.cxx:
            status("setting cxx compiler to '%s'." % self.config.cxx, 2)
            self.lines.append("CXX = %s" % self.config.cxx)

        if self.config.ar:
            status("setting archive tool to '%s'." % self.config.ar, 2)
            self.lines.append("AR = %s" % self.config.ar)

        if self.config.make:
            status("setting make to '%s'." % self.config.make, 2)
            self.lines.append("MAKE = %s" % self.config.make)

        if self.config.shell:
            status("setting shell to '%s'." % self.config.shell, 2)
            self.lines.append("SHELL = %s" % self.config.shell)

        status("setting build dir to '%s'." % self.config.output_dir, 2)
        self.lines.append("\nBUILD_DIR = %s" % self.config.output_dir)

        defaults = self.get_defaults()
        debug_flags = ' '.join(self.config.debug_flags)
        self.lines.append(HEADER_2 % (defaults, debug_flags, defaults))

    def get_defaults(self):
        defaults = []

        for target in self.config.targets.keys():
            status("adding '%s' as a default target." % target, 2)
            defaults.append('%s/%s' % (self.config.output_dir, target))

        return ' '.join(defaults)

