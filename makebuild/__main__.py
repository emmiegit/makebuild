#!/usr/bin/env python
#
# __main__.py
#
# makebuild - A tool to create simple and portable Makefiles.
# Copyright (c) 2015-2016 Ammon Smith
#
# makebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# makebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with makebuild.  If not, see <http://www.gnu.org/licenses/>.
#

import os.path
import sys

if __package__ is None and not hasattr(sys, "frozen"):
    path = os.path.realpath(os.path.abspath(__file__))
    sys.path.insert(0, os.path.dirname(os.path.dirname))

import makebuild

if __name__ == "__main__":
    makebuild.main(sys.argv)


