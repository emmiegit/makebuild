## makebuild
A tool to create simple and portable Makefiles.

This program generates Makefiles for C/C++ projects in the way I usually create them, but does so in an automated way to avoid manually typing boilerplate. It seeks to provide many of the nice features that build tools provide, such as recompilation on header updates. This is for projects that want to use a normal Makefile to build their project to avoid compilation dependencies like autotools or CMake.

Available under the terms of the GPLv2+.

### Running
`makebuild` supports both Python 2 and Python 3.
```
python -m makebuild [-v] [-o Makefile] [makebuild.json]
```

### Configuration
The configuration file is assumed to be `makebuild.json`. It is in the following format:
Items in angle brackets are mandatory.
```json
{
    "project": {
        "name": <string>,
        "version": <string>,
        "header": (path to text file, default no Makefile header)
    },

    "build": {
        (default means use whatever make uses),
        "c-compiler": (default null),
        "cxx-compiler": (default null),
        "archiver": (default null),
        "make": (default null),
        "shell": (default null),

        "output-dir": (default "build"),

        "include-dirs": [
        ],
        "flags": [
        ],
        "compile-flags": [
        ],
        "link-flags": [
        ],
        "debug-flags": [
            (default "-g")
        ],

        "sources": [
            (list of files and directories, default "src")
        ],

        (these flags are inherited by individual targets, who can add to them)
        "c": {
            "include-dirs": [
            ],
            "flags": [
            ],
            "compile-flags": [
            ],
            "link-flags": [
            ]
        },
        "cxx": {
            "include-dirs": [
            ],
            "flags": [
            ],
            "compile-flags": [
            ],
            "link-flags": [
            ]
        {,

        "targets": {
            (name): {
                "sources-include": [
                ],
                "sources-exclude": [
                ],

                "include-dirs": [
                ],
                "flags": [
                ],
                "compile-flags": [
                ],
                "link-flags": [
                ],

                "c": {
                    "include-dirs": [
                    ],
                    "flags": [
                    ],
                    "compile-flags": [
                    ],
                    "link-flags": [
                    ]
                },
                "cxx": {
                    "include-dirs": [
                    ],
                    "flags": [
                    ],
                    "compile-flags": [
                    ],
                    "link-flags": [
                    ]
                },

                "output": (name of object, default same as name),
                "type": <'executable', 'static-lib', 'shared-lib'>
            }
        }
    },

    "misc": {
        (optional, defaults are listed)
        "extensions": {
            "default": ('c' or 'cxx', default 'c'),
            "c-sources": [
                "c"
            ],
            "c-headers": [
                "h"
            ],
            "cxx-sources": [
                "cc",
                "cxx",
                "cpp",
                "c++",
                "C"
            ],
            "cxx-headers": [
                "hh",
                "hxx",
                "hpp",
                "h++",
                "H",
                "ii",
                "ixx",
                "ipp",
                "inl",
                "txx",
                "tpp",
                "tpl"
            ]
        }
    }
}
```

